// We are importing writable for to use the Store of Svelte

import { writable } from "svelte/store"

// We see if the user no have a colorTheme in the localStorage, and if colorTheme === null we assign for default light

if(localStorage.getItem("colorTheme") === null) {
    localStorage.setItem("colorTheme", "light")
} else {
}

// We assign the value of localStorage a variable

let localStorageTheme = localStorage.getItem("colorTheme")

// We are exporting the value for can use like a store

export const themeColor = writable(localStorageTheme)

// We are does the validation for to assign in localStorage with a subscription of the store

themeColor.subscribe(value => {
	localStorage.setItem("colorTheme", value === "dark" ? "dark" : "light")
})
