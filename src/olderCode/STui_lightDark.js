import { writable } from 'svelte/store';

// Theme mode
export const lightDark = writable("light");