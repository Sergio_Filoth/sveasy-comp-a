// Reference from https://www.w3schools.com/howto/howto_css_switch.asp

import {writable} from 'svelte/store'

export const mode = writable("L");

export const general = writable({
    'primary':'#3F6AD8',
    'secundary':'#6C757D',
    'success':'#3AC47D',
    'info':'#16AAFF',
    'warning':'#F7B924',
    'danger':'#D92550',
    'focus':'#444054',
    'alt':'#794C8A',
    'light':'#EEEEEE',
    'dark':'#343A40',
    'link':'#3F6AD8',
    'primaryDis':'#90A9E9',
    'secundaryDisable':'#A0A6AB',
    'successDisable':'#7FD9AB',
    'infoDisable':'#68C8FF',
    'warningDisable':'#FAD270',
    'dangerDisable':'#E77994',
    'focusDisable':'#868390',
    'altDisable':'#A88BB3',
    'lightDisable':'#EEEEEE',
    'darkDisable':'#7C7F83',
    'linkDisable':'#A0A6AB',
    'white':'#FFFFFF',
    'black':'#000000',
    'shadow_1':'-webkit-box-shadow: 0px 6px 10px 3px rgba(176,176,176,1); -moz-box-shadow: 0px 6px 10px 3px rgba(176,176,176,1); box-shadow: 0px 6px 10px 3px rgba(176,176,176,1);',
    'shadow_2':'',
    'brp_xs':'0px',
    'brp_sm':'576px',
    'brp_md':'768px',
    'brp_lg':'992px',
    'brp_xl':'1200px',
})

export const light = writable({
    'input_bg_primary':'#FFFFFF',
    'input_bg_secundary':'#CDCDCD',
    'input_text_title':'#393D3F',
    'input_text':'#454A4D',
    'input_text_help':'#6B7278',
    'input_border_color':'#BFBFBF',

    'header_bg':'#FAFBFC',

    'menuFloat_bg':'#FFFFFF',
    'menuFloat_bg_hover':'#E0F3FF',

    'content_bg_top':'#EFF1F2',
    'content_bg':'#E3E6E8',

    'card_bg_header':'#D7DCE0',
    'card_bg_body':'#FAFBFC',
    
    'txt_color':'#1D1F20',

    'table_bg_Title':'#FAFBFC',
    'table_bg_RowOdd':'#FAFBFC',
    'table_bg_RowEven':'#E6ECF0',
    'table_bg_hover':'#E0F3FF',
    
    'border':'#D7DCE0',

    'menuMain_bg':'#FAFBFC',
    'menuMain_bg_hover':'#E0F3FF',
    'menuMain_icon':'#DDDDDD',
    'menuMain_text_select':'#3F6AD8',
})

export const dark = writable({
    'input_bg_primary':'#1D1F20',
    'input_bg_secundary':'#232627',
    'input_text_title':'#BDB8B0',
    'input_text':'#BDB8B0',
    'input_text_help':'#BDB8B0',
    'input_border_color':'#393D3F',
    'input_inset_shadow':'inset 0 0 1px rgba(0,0,0,0.2);',
    'input_outset_shadow':'0 3px 26px rgba(0, 0, 0, 0.15);',

    'header_bg':'#1F2123',

    'menuFloat_bg':'#1D1F20',
    'menuFloat_bg_hover':'#454A4D',

    'content_bg_top':'#202323',
    'content_bg':'#232627',

    'card_bg_header':'#26282A',
    'card_bg_body':'#1D1F20',

    'txt_color':'#BDB8B0',

    'table_bg_Title':'#1D1F20',
    'table_bg_RowOdd':'#26282A',
    'table_bg_RowEven':'#2E3133',
    'table_bg_hover':'#454A4D',
 
    'border':'#393D3F',

    'menuMain_bg':'#1D1F20',
    'menuMain_bg_hover':'#26282A',
   'menuMain_icon':'#494948',
    'menuMain_text_select':'#3F6AD8',

})

